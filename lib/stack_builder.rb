
require_relative "stack_builder/version"
require_relative "stack_builder/inventory"
require_relative "stack_builder/stack"
require_relative "stack_builder/handler"

include StackBuilder

module StackBuilder
  $template_path = File.expand_path File.dirname(__FILE__)

  #
  # Public: Read the yaml config file. Used for DB credentials and internal mapper constants
  #
  # Examples
  #
  #   config['dev_db']
  #   # => { db credentials hash }
  #
  # Returns the yaml content as hash.
  def config
    YAML::load( File.open('/etc/ouzzo/dbconfig.yml'))
  end

  #
  # This gives methods for array means.
  # Poor mans way without extending Object::Array core classes
  #
  # Fast Array Sum
  def sum
    raise ' Caller is not an Array ' unless self.instance_of? Array
    inject(0, :+)
  end

  # simple mean
  #
  def mean
    raise ' Caller is not an Array' unless self.instance_of? Array
    sum / size
  end






end
