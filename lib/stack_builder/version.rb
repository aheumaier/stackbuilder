module StackBuilder
  # Gem version number
  VERSION = "0.0.1"

  #
  # Prints Gem VERSION Constant
  #
  # Examples
  #
  #   output_version
  #   # => "0.0.1"
  #
  # Returns the VERSION Constant.
  def output_version
    puts StackBuilder::VERSION
  end
end