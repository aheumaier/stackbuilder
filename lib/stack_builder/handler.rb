require 'tree'
require 'aws-sdk'

module StackBuilder
  class Handler

    attr_reader :stacks

    def initialize stacks
      @stacks = stacks
      @region = 'as-east-2'
      begin
        @run_order  = [] # build_tree
        @cf_stack = Aws::CloudFormation::Client.new(region: @region)
      rescue Aws::CloudFormation::Errors::ServiceError => e
        raise e
      end

    end

    private

    def run command
      system(command)
    end

    def launch
      @cf_stack.create_stack({
                                 stack_name: "StackName", # required
                                 template_body: "TemplateBody",
                                 template_url: "TemplateURL",
                                 parameters: [
                                     {
                                         parameter_key: "ParameterKey",
                                         parameter_value: "ParameterValue",
                                         use_previous_value: true,
                                     },
                                 ],
                                 disable_rollback: true,
                                 timeout_in_minutes: 1,
                                 notification_arns: ["NotificationARN"],
                                 capabilities: ["CAPABILITY_IAM"], # accepts CAPABILITY_IAM
                                 on_failure: "DO_NOTHING", # accepts DO_NOTHING, ROLLBACK, DELETE
                                 stack_policy_body: "StackPolicyBody",
                                 stack_policy_url: "StackPolicyURL",
                                 tags: [
                                     {
                                         key: "TagKey",
                                         value: "TagValue",
                                     },
                                 ],
                             })
    end

  end
end
