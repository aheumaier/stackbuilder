require 'yaml'

module StackBuilder
  class Stack

    attr_reader :name, :path, :depends, :buildcommand

    def initialize path
      begin
        @config = File.absolute_path path
        @path   = File.dirname(File.absolute_path path)
        @stack_params = load_config(@config)
        @name = @stack_params['name']
        @depends = @stack_params['depends']
        @buildcommand = @stack_params['buildcommand']
      rescue StandardError => e
        puts ''
        puts  'FATAL: '+e.message
        puts 'Cannot proceed ...'
        exit 1
      end
    end

    def parent?
      if File.exist? parent_stack_path
        load_config(parent_stack_path)['name']
      else
        'root'
      end

    end

    private

    def parent_stack_path
      parent_dir(@path)+'/.stackbuilder'
    end

    def parent_dir path
      path.split('/')[0...-1].join('/')
    end

    def load_config path
      begin
        YAML.load_file path
      rescue StandardError => e
        puts ''
        puts  'FATAL: '+e.message
        puts 'Cannot proceed ...'
        exit 1
      end
    end

  end
end