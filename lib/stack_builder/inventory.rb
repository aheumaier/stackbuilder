# This class build an inventory of known stacks and default configs in the current directories
#
module StackBuilder
  class Inventory


    attr_reader :stacks, :stack_names, :tree

    def initialize
      print 'Building Inventory'
      @stacks = []
      @stack_names = []
      find_stacks()
      @tree = build_tree
    end

    def validate_dependencies
      self.stacks.map do |stack|
        unless stack.depends.nil?
          stack.depends.each do |dep|
            unless @stack_names.include? dep
              puts 'failed dependency for '+ dep+ ' in '+ stack.name
              exit 1
            end
          end
        end
      end
    end

    private

    def find_stacks
      Dir.glob('**/.stackbuilder').each do |stack|
        add stack
      end
      self.stacks.each { |stack| @stack_names << stack.name }
      puts ''
    end

    def add stack_path
      print '.'
      @stacks << StackBuilder::Stack.new( stack_path )
    end


    def build_tree
      @tree = Tree::TreeNode.new("rootstack", find_root )
      find_children(@tree)
      @tree
    end

    def find_root
      @stacks.each do |s|
        if s.parent? == 'root'
          return s
        end
      end
    end

    def find_children parent_stack
      @stacks.each do |stack|
        if stack.parent? == parent_stack.name
          p = find_node stack.parent?
          p << Tree::TreeNode.new(stack.name, stack)
          find_children stack
        end
      end

    end

    def find_node node_name
      @tree.breadth_each do |node|
        if node.name == node_name
          return node
        end
      end
    end

  end
end