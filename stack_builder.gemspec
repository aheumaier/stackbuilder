# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'stack_builder/version'

Gem::Specification.new do |spec|
  spec.name          = "stack_builder"
  spec.version       = StackBuilder::VERSION
  spec.authors       = ["Andreas Heumaier"]
  spec.email         = ["andreas.heumaier@nordcloud.com"]
  spec.summary       = %q{Install AWS Cloudfomation Stacks the easy way}
  spec.description   = %q{Install AWSCloudfomation Stacks the easy way}
  spec.homepage      = "http://nordcloud.com"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "json"
  spec.add_development_dependency "thor"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "aws-sdk"
  spec.add_development_dependency "aws-sdk-core"
  spec.add_development_dependency "aws-sdk-resources"
  spec.add_development_dependency "rubytree"
end
